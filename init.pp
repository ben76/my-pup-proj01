class network {
 file { "/etc/resolv.conf":
  owner => root,
  group => root,
  mode => 644,
  source => "puppet:///network/resolv.conf"
 }
}
