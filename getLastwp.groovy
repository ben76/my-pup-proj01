/**
 * Created by Ben on 07/02/2018. à 25h00
 * argument supplimenatire : +dif  ou +diffcom pour actioner la solution par diff commit
 */
import hudson.model.*
import java.lang.reflect.Array
import java.util.List
import java.io.File
import groovy.json.*
import java.nio.file.*


//     SCRIPT in JOB
// java -jar /home/ben/jenkins_apprendre/jenkins-cli.jar -remoting -s http://localhost:8080 groovy $WORKSPACE/getLastwp.groovy $WORKSPACE/"liste-wpar.json" +dif --username admin --password 'admin'
//
//def globalWparAffectFile = './liste-wpar.json'
//def wparRecentUpFile = './wpar-lastupdated-list.json'
//      variables globales
// attribRech ='"host"' //attribut à rechercher ; pour une utilisation future

// Variables globales
workspace=""                    // repertoire de travail
globalWparAffectFile=""      // fichier qui contien la liste des wpars  ex: liste-wpar.json
wparRecentUpFile=""         // le fichier  output
mode="comp2vers"              //mode de solution : comparaison des deux version , l'autre mode est mode= diffcom : diff commit
if (args.length != 0) {
    if ((args.length >1) && (args[(args.length -1)].startsWith("+dif") || args[(args.length -1)].startsWith("+diffcom") ))   {  // 2 arg diffcom
        mode="diffcom"
    }
    workspace= Paths.get(args[0].toString()).getParent().toString()  // deduire le repertoire de travail du chemin fichier
    globalWparAffectFile =  args[0]
    wparRecentUpFile = "${workspace}/wpar-lastupdated-list.json"
}else{
    println "### Erreur :  Pas de fichier valide en argument [0], veuillez saisir un nom de fichier valide"
    return
}


// Methode : parserDiffCommitToList(diffCommit)
// Input : le text du diff commit du fichier json des wpars

def parsDiffCommitToList(def diffCommit){
    def hostsList=[]
    diffCommit.eachLine{ line ->
        if (line.contains('"host"')) {  // rechercher toute occurence de mot host
            if (! line.startsWith('- ')){
                hostsList << (line.tokenize(':" '))[1]  // parser pour avoir un resultat sous format: [host, sic-dev-tmsl8-s90]
            }
            else{
                print "la ligne host= ${trouv} a été modifiée ou supprimée du fichier waprlist \n ce qui n'est pas d'usage... "
            }
        }
    }
    hostsList.isEmpty()? println(" --> Aucun host modifié de manière significative n'a été trouvé")  :  println(" --> Hosts modifiés trouvés " +hostsList )
    return  hostsList   //resultat soit null soit une liste...
}

// Methode : getWparListFromCible(input : wpar-affecatation, liste des hosts parsées)
// input  : ( fichier source json des WPARS, liste des hosts parsées du diff commit)
// output :  ArrayList des des WPARS affactees réellement

def getWparListFromCible(def cibleFile, def hostsList){
    def wpRealAffectList=[]
    if (hostsList!= null){ //élément trouvé
        // Recuperer la liste globale depuis wpar-affectation
        File fileContents = new File(cibleFile)
        if (!fileContents.exists())
            println "### Erreur, impossible de trouver le fichier " + cibleFile //cause : fichier Json introuvable
        else{
            def InputJSON = new JsonSlurper().parseText(fileContents.text)
            def globWparList = InputJSON.wparlist
            if ( globWparList == null){ // cause : fichier Json vide ou altere
                println "Erreur : le fichier ${cibleFile} est alteré ou vide..."
                return
            }

            hostsList.each{ hostAft ->
                def wparFnd = globWparList.find{ wpar ->
                    (hostAft == wpar.host) // comparaison par nom de host
                }
                if (wparFnd != null )
                    wpRealAffectList << wparFnd
                else
                    println "Erreur, le champs <host> manipulé mais introuvable dans le fichier Json global des WARS ${cibleFile}... "
            }
        }
    }else{
        println "Aucun changement significatif n'a été detecté sur le fichier des wparlist"
    }
    return wpRealAffectList
}



// git diff HEAD^ HEAD wpar-affectation.json > $diffBtwCommits

//methode GIT diff avec groovy si on ne veut pas utilise shell
// output : ([code_err, msg] si code_err==0 pas pb)

def myGit(def command, def args){

    // Execution de la commande git
    println "  git -C ${workspace} ${command} ${args}"    // msg pour deboguer
    Process process = "git -C ${workspace} ${command} ${args}".execute()
    def err = new StringBuilder()
    def out = new StringBuilder()
    //process.consumeProcessOutput( out, err )
    //process.waitFor()
    process.waitForProcessOutput(out, err)
    if( out.size() > 0 ) println out
    if( err.size() > 0 ) println err

    if (err.length()==0) return ([code_err:0, msg:out.toString()])
    else return ([code_err:1, msg:err.toString()])
}

// test si le fichier wpar-affectation est déjà modifié
// output: ([code_err, msg] si code_err==0 pas pb)
def getDiffCommits(String file){
    def resultDif

    println "inside getDiffCommits : la valeur de file == "+ file       //pour debug

    ////git diff --name-only HEAD~1..HEAD
    def testChg = myGit("diff", "--name-only HEAD~1..HEAD")
    if(testChg.code_err == 0){
        println "..... testChg.code_err == 0  et file = "+file   //pour debug

        if (testChg.msg == null){
            println " --> Aucun changement significatif n'a été detecté au niveau du fichier ${file}"
        }else{
            println "testChg.msg == " + testChg.msg
            if (testChg.msg.toString().contains(Paths.get(file.toString()).getFileName().toString())) {
                println "... After testChg.msg.Contains(fileName)"

                //resultGit2 = myGit("diff", "HEAD^^ HEAD ${nomfichier}")
                resultDif = myGit("diff", "HEAD~1..HEAD ${file}")
                println "... Apres le myGit diff HEAD~1..HEAD ....  "
                println resultDif

                if (resultDif.code_err == 0) return ([code_err:0, "msg":resultDif.msg]) //return le diff
                else {
                    println "### Erreur lors de l'exécution du : git diff ..." + testChg.err.toString() //retrun err
                    return ([code_err: 1, msg: resultDif.msg])
                }
            }else{
                return ([code_err: -1, msg: " --> Le fichier n'a pas été affecté "]) //code_err: -1 : no error, no file changed
            }
        }

    }else { //return err
        println "### Erreur : Git introuvable ou repertoire non reinitialisé"   //pour debug
        return ([code_err: 1, msg: "Erreur : Git introuvable ou repertoire non reinitialisé \n" + testChg.err.toString()])
    }
}//end method


// Recuperer le fichier de la version précédente
//output: get Previous version File en format String
def getFileOfPreviousVers(def file){

    // execution de la commande : git show HEAD~1:fichier.json
    def resultPrev = myGit("show", "HEAD~1:${Paths.get(file.toString()).getFileName().toString()}")  //only file name, no Path
    //println("..... in in in previous file for the last commit  after myGit\n " + resultGit1 )
    if(resultPrev.code_err == 0){
        println "..... resultPrev.code_err == 0 "                           //pour debug
        println "Recupération de la version précedente du fichier : "+ file //pour debug
        //println "resultPrev.msg == " + resultPrev.msg
        return  ([code_err:0,msg:resultPrev.msg])
    }else //return err
        return  ([code_err:1,msg: "### Erreur : Git introuvable ou repertoire non reinitialisé \n" + resultPrev.err.toString()])
}


// Compare les deux derniers dernieres version du fichier en entrée exemple : wpar-list.json
// Output: affectedWparList Arraylist des wpars affectée depuis last commit
// Exemple utilisation :  compareBtwVersionAllByAll('liste-wpar.json')

def compareBtwVersionAllByAll(def fileCible){
    def affectedWparList  = [] // contiendra seulement les wpar récement affectés
    // recupère l'actuelle version du fichier JSON wparlist
    def currentInptSON = new JsonSlurper().parseText((new File(fileCible).text))//currFileContents)
    def currentWparList = currentInptSON.wparlist

    try{
        if (currentWparList == null)
            println "### Erreur, Fichier wparlist non valide ou corrmpu... "
        else{
            println " -> currentWparList existe,  differente de null."
            try{
                def previousFile = getFileOfPreviousVers(globalWparAffectFile) //
                //println(" après le getDiffCommits()... rdifComm == \n" + previousFile)
                if (previousFile.code_err == 0){ //aucune erreur ;
                    //conversion du fichier en ArrayList par JsonSlurper
                    def previousInptSON = new JsonSlurper().parseText(previousFile.msg)
                    def previousWparList = previousInptSON.wparlist       //ArrayList pour la version actuel
                    println " --> Comparaison en cours entre les 2 versions..."
                    println " --> Recheche que les WPARs affectée(s) après le dernier commit..... "
                    currentWparList.eachWithIndex{ wpar, idx ->
                        if (! previousWparList.contains(wpar)){    //on prend seulement les wpars qui ne sont pas dans l'ancienne version
                            println wpar
                            affectedWparList << wpar
                        }
                    }
                }else{
                    println "### Erreur lors de l opération, impossible de recuperer la version précédente du fichier : " + 'list-wpar.json'
                }
            }catch(Exception e){
                println "### Erreur : problème d accès aux fichiers ... vérifier les droits ou que le fichier exist\n"+ e.toString()
                //return
            }
        }
    }catch (Exception e){
        println "### Erreur : fichiers liste-wpar.json et ..fichier inexistant ou acces refusé.\n vérifier les droits ou que le fichier exist\n"+ e.toString()
        //return
    }finally{
        // Retourne la List affectedWparList même si elle est vide
        return affectedWparList
    }
}


// Methode : setToJsonFileTarget() : Génération du fichier json que les WPARs récement modifiées
// Input : ArrayList des WPARs recement modifier
// Output : fihchier Json

def setToJsonFileTarget(def targetFile, def wpRealAffectList){
    def json = JsonOutput.toJson(wpRealAffectList)
    //println(" --> Making json file de la Liste des WPARs fraichement affectées ...")
    new File(targetFile).write('{"wparlist": ' + json +'}' ) //resultat enregistré dans un fichier
}

//---------------------------------------------------------------------
//                             Main
//---------------------------------------------------------------------

if (mode != "diffcom"){ //solution par défaut : comp2vers

    // 1ere Solution la plus classique comprer les 2 dernières version du fichier 'liste-wpar.json'
    println " ==> MODE : comp2vers - solution par défaut basée sur la comparaison des 2 versions du fichier wparlist..."


    def wparRecentAffectList = compareBtwVersionAllByAll(globalWparAffectFile)   // en occurrence : 'liste-wpar.json'
    if ( wparRecentAffectList.isEmpty()){
        println " --> Aucun changement significatif n'a été détecté dans la liste des WPARs ...!! "
        wparRecentAffectList=[] //créer un fichier output vide si no wpars detectée
        setToJsonFileTarget(wparRecentUpFile, wparRecentAffectList)
        println " --> Fichier : ${wparRecentUpFile} est créé mais ne contient aucune WPAR !!" //choix volontaire

    }else{
        println " --> ${wparRecentAffectList.size()} WPARs affectée(s) trouvée(s). "
        setToJsonFileTarget(wparRecentUpFile, wparRecentAffectList)
        println " --> Fichier de format json   ${wparRecentUpFile} est créé dans le repertoir de travail"
    }


}else{      
    //              2e Solution
    //  utilisation juste du diff commit pour et déduire les wpars récement affectées
    // Pour utuliser la 1e solution : Décommenter cette partie et commenter la 2e solution.
        println " ==> MODE : diffcom (+dif) ; solution basée sur le parsing du diff commit ..."

    def hostsList = []
    def wparRecentAffectList = []
     def msgsupp = ""
    try{
        def rdifComm = getDiffCommits(globalWparAffectFile) //
        //println(" après le getDiffCommits()... rdifComm == \n" + rdifComm)

        if (rdifComm.code_err == 0){    //aucune erreur

            //recuperer le resulat du diff
            //parser le resultat et deduire la liste wparAddedList, wparDeletedList, wparModifyedList (ou Updated)
            hostsList = parsDiffCommitToList(rdifComm.msg)
            wparRecentAffectList = getWparListFromCible(globalWparAffectFile, hostsList)

        }else
        if (rdifComm.code_err == -1) println "--> Le dernier commit ne semble pas affecter le fichier des wparlist."
        else println "### Erreur lors de l opération: diff commit ..."

    }catch(Exception e){
        println "### Erreur : Probleme d accès aux fichiers ... vérifier les droits ou que le fichier exist\n "+ e.toString()
        //return
    }

    if ( wparRecentAffectList.size() != 0){
        println " ------->>  ${wparRecentAffectList.size()}  WPARs affectées depuis le dernier commit : <<-------"
        println wparRecentAffectList
        msgsupp = "\n Il contient : ${wparRecentAffectList.size()}  WPARs affectée(s)"

    }else{
        println " --> Aucune WPAR affectée n'est trouvée lors du parsing du -diff commit"
        msgsupp =" \n Mais le fichier est vide et ne contient aucune WPAR affectée...!!"
    }

    // generer un fichier json des WPARs affectées récement entre les 2 derniers commits
    // meme si aucune WPAR trouvé, on le vide.
    setToJsonFileTarget(wparRecentUpFile, wparRecentAffectList)

    println " --> Fichier : ${wparRecentUpFile} est créé..." + msgsupp
    

}
